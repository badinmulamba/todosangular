import { Component, OnInit } from '@angular/core';
import { Todo } from '../../models/todos';


@Component({
  selector: 'app-todos',
  templateUrl: './todos.component.html',
  styleUrls: ['./todos.component.css']
})
export class TodosComponent implements OnInit {

  todos : Todo[];

  constructor() { }

  ngOnInit(): void {

    this.todos = [
      {
        name : "badin",
      },
      {
        name : "angel",
      },
      {
        name : "akeala",
      }
    ];
  }

  addname(newPerson) {
    var newName = {
      name : newPerson,
    }
    this.todos.push(newName)
  }

  deleteName(item) {
    this.todos = this.todos.filter(x => x.name !== item.name)
  }
}
